const addBtn = document.querySelector('#add-btn');
const srcBtn = document.querySelector('#src-btn');
const addInp = document.querySelector('#add-inp');
const srcInp = document.querySelector('#src-inp');
const ans = document.querySelector('#ans');
let dict = new Set();

addBtn.addEventListener('click', e => {
    e.preventDefault();
    dict.add(addInp.value);
    addInp.value = '';
});

srcBtn.addEventListener('click', e => {
    e.preventDefault();
    if (dict.has(srcInp.value)) {
        ans.innerText = `Word "${srcInp.value}" exist!`;
    } else {
        ans.innerText = `Word "${srcInp.value}" doesn't exist!`;
    }
    srcInp.value = '';
});
